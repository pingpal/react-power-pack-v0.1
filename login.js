
var $ = require('jquery')
var React = require('react')

var m = require('./merge')
var find = require('./find')

var Whatevs = require('./whatevs')

require('./shake')

var styles = {}

styles.box = {
    display: 'block',
    width: 360,
    margin: '0 auto',
    background: '#fff',
}

styles.row = {
    position: 'relative'
}

styles.btn = {
    display: 'block',
    textDecoration: 'none',
    padding: 15,
    background: '#079DD9',
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    cursor: 'pointer'
}

styles.btn_press = {
    background: '#037dca'
}

styles.input = {
    padding: '31px 10px 29px 50px',
    border: 'none',
    borderRadius: 0,
    background: 'transparent'
}

styles.inputTop = {
    borderBottom: 'solid 1px #eee'
}

styles.inputBottom = {
    borderBottom: 'solid 1px #eee'
}

styles.icon = {
    position: 'absolute',
    top: 23,
    left: 20,
    color: '#999'
}

styles.noglow = {
    outline: 'none',
    // border: 'none !important',

    shadow: 'none !important',
    MozBoxShadow: 'none !important',
    WebkitBoxShadow: 'none !important'
}

styles.submit = {
    position: 'absolute',
    top: '-9999px'
}

styles.button = {
    position: 'absolute',
    top: 20,
    right: 20,
    // color: '#999'
}

styles.btnDepth = {
    display: 'inline-block',

    marginBottom: 0,
    padding: '1px 5px 1px 5px',

    fontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
    color: 'rgb(255, 255, 255)',
    fontSize: 12,
    fontWeight: 900,
    lineHeight: '18px',
    textAlign: 'center',
    textDecoration: 'none',
    cursor: 'pointer',

    borderRadius: 2,
    background: '#079DD9',
    boxShadow: '#497591 0px 2px 0px 0px'
}

var HoverButton = React.createClass({
    getInitialState: function () {
        return { hover: false }
    },

    mouseDown: function () {
        this.setState({ hover: true })
    },

    mouseUp: function () {
        this.setState({ hover: false })
    },

    render: function() {

        var style = m(/*styles.row,*/ styles.btn, this.props.styleBtn, this.state.hover && styles.btn_press, this.state.hover && this.props.styleBtnPress)

        return <a style={style} onClick={this.props.handleLogin} onMouseDown={this.mouseDown} onMouseUp={this.mouseUp}>{this.props.text || 'Sign in'}</a>
    }
});

var Login = React.createClass({
    shakeBox: function () {
        $(find(this, 'shake')).cSSShake()
    },
    componentWillUnmount: function () {
        this.state.whatevs.off(null, this)
    },
    componentWillMount: function() {
        this.state.whatevs.on('shake', this, function (selected) {
            this.shakeBox()
        })
        this.state.whatevs.on('clear', this, function (selected) {
            find(this, 'password').value = find(this, 'username').value = ''
        })
    },
    getInitialState: function () {
        return { widthBtnUser: 0, widthBtnPass: 0, whatevs: this.props.whatevs || new Whatevs() }
    },
    componentDidMount: function () {
        var btnUser = find(this, 'btnUser'), btnPass = find(this, 'btnPass')

        btnUser && (this.state.widthBtnUser = btnUser.offsetWidth)
        btnPass && (this.state.widthBtnPass = btnPass.offsetWidth)

        this.setState(this.state)
    },
    handleLogin: function (e) {
        e.preventDefault()

        var username = find(this, 'username').value.trim()
        var password = find(this, 'password').value.trim()

        /*
        if (!username || !password) {
            this.shakeBox()
            return
        }
        */

        this.props.handleLogin(username, password)/* || this.shakeBox()*/
    },
    handleBtnUserClick: function () {
        this.props.onBtnUserClick && this.props.onBtnUserClick()
    },
    handleBtnPassClick: function () {
        this.props.onBtnPassClick && this.props.onBtnPassClick()
    },
    handleInputFocus: function () {
        this.props.onInputFocus && this.props.onInputFocus()
    },
    render: function () {

        var styleInputUser = m(styles.noglow, styles.input, this.state.widthBtnUser && { paddingRight: this.state.widthBtnUser + 30 }, styles.inputTop, this.props.styleInput, this.props.styleInputTop)
        var styleInputPass = m(styles.noglow, styles.input, this.state.widthBtnPass && { paddingRight: this.state.widthBtnPass + 30 }, styles.inputBottom, this.props.styleInput, this.props.styleInputBottom)

        return (
            <form ref="shake" style={m(styles.box, this.props.styleBox)} onSubmit={this.handleLogin}>
                <div style={styles.row}>
                    <span style={styles.icon} className="fa fa-user"></span>
                    <input ref="username" type="text" className="form-control" style={styleInputUser} placeholder="Username" onFocus={this.handleInputFocus}/>
                    { this.props.btnUser && <a href="#" tabIndex="-1" ref="btnUser" style={m(styles.button, styles.btnDepth)} onClick={this.handleBtnUserClick}>{this.props.btnUser}</a> }
                </div>
                <div style={styles.row}>
                    <span style={styles.icon} className="fa fa-unlock-alt"></span>
                    <input ref="password" type="password" className="form-control" style={styleInputPass} placeholder="Password" onFocus={this.handleInputFocus}/>
                    { this.props.btnPass && <a href="#" tabIndex="-1" ref="btnPass" style={m(styles.button, styles.btnDepth)} onClick={this.handleBtnPassClick}>{this.props.btnPass}</a> }
                </div>
                <HoverButton {...this.props} handleLogin={this.handleLogin} />
                <input type="submit" style={styles.submit}/>
            </form>
        );
    }
});

module.exports = Login
