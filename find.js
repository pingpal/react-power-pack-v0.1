var ReactROM = require('react-dom')

function find (that, ref) {
    return ref ? ReactROM.findDOMNode(that.refs[ref]) : ReactROM.findDOMNode(that)
}

module.exports = find
