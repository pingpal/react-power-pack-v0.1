var React = require('react')
var $ = require('jquery')

var m = require('./merge')
var find = require('./find')
var Whatevs = require('./whatevs')

var styles = {}

styles.control = {
    position: 'relative',
    zIndex: 1,
    height: 50,
    background: '#fff',
    border: 'solid 1px #eee'
}

styles.label = {
    position: 'relative',
    height: 50,
    padding: '15px 0 0 20px',
    color: '#999',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    cursor: 'pointer'
}

styles.label_selected = {
    color: '#333'
}

styles.input = {
    position: 'absolute',
    top: 0,
    left: 0,
    height: 48,
    padding: '1px 0 0 20px',
    color: '#333',
    border: 'none',
    outline: 'none'
}

styles.button = {
    float: 'right',
    position: 'relative',
    top: -1,
    right: -1,
    width: 50,
    height: 50,
    marginLeft: 20,
    marginBottom: -1,
    background: '#079DD9',
    cursor: 'pointer'
}

styles.button_press = {
    background: '#037dca'
}

styles.arrow = {
    width: 0,
    height: 0,
    margin: '22px auto 0 auto',
    borderLeft: '7px solid transparent',
    borderRight: '7px solid transparent',
    borderTop: '7px solid #fff'
}

styles.options = {
    border: 'solid 1px #eee',
    borderTop: 'none',
    borderBottom: 'none',
    background: '#fff',
    boxShadow: '0 3px 10px rgba(0, 0, 0, 0.16)'
}

styles.option = {
    height: 50,
    width: '100%',
    padding: '15px 20px 0 20px',
    borderBottom: 'solid 1px #eee',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    cursor: 'pointer'
}

styles.option_hover = {
    background: '#f5f5f5'
}

styles.option_active = {
    background: '#eeeeee'
}

styles.select = {
    position: 'relative',
    zIndex: 100000,
    height: 50,

    userDrag: 'none',
    userSelect: 'none',
    MozUserDrag: 'none',
    MozUserSelect: 'none',
    WebkitUserDrag: 'none',
    WebkitUserSelect: 'none'
}

styles.tab = {
    outline: 0
}

var Control = React.createClass({
    jump: false,
    
    getInitialState: function () {
        return { selected: null, press: false }
    },
    componentWillUnmount: function () {
        this.props.whatevs.off(null, this)
    },
    componentWillMount: function() {
        this.selectionDidOccur(this.props.selected || { value: -1, name: '' })
        this.props.whatevs.on('select', this, function (selected) {
            this.selectionDidOccur(selected)
        })
        this.props.whatevs.on('-grab-selected', this, function () {
            this.props.onChange && this.props.onChange(this.state.selected && this.state.selected.name)
            return this.state.selected
        })
        this.props.whatevs.on('clear-selected', this, function () {
            this.setState({ selected: { value: -1, name: '' } })
            this.props.onCloseOptions && this.props.onCloseOptions()
        })
    },
    componentDidMount: function () {
        this.width = find(this, 'label').offsetWidth
    },
    componentDidUpdate: function () {
        this.width = find(this, 'label').offsetWidth

        // bug apptimate/safewire-client#16
        //if (this.props.open) {
        //    var el = find(this, 'input')

        //    el.focus()
        //    var val = el.value
        //    el.value = ''
        //    el.value = val
        //}
    },
    selectionDidOccur: function (selected) {
        this.setState({ selected: selected })
    },
    stopPropagation: function (e) {
        e.stopPropagation()
    },
    mouseUp: function () {
        this.setState({ press: false })
        this.props.onToggleOptions()
    },
    mouseDown: function () {
        this.setState({ press: true })
    },
    mouseOut: function () {
        this.setState({ press: false })
    },
    keyDown: function (e) {
        if (e.key == 'Enter') {
            var handled = this.props.whatevs.exe('enter')

            console.log('enter!')

            if (!handled) {
                console.log('not handled!')
                this.props.onChange && this.props.onChange(this.state.selected && this.state.selected.name)
                this.props.onSubmit && this.props.onSubmit(this.state.selected)
                this.props.onCloseOptions && this.props.onCloseOptions()
            } else {
                this.jump = true
                find(this, 'tab').focus()
            }
        }

        if (e.key == 'ArrowUp') {
            this.props.whatevs.exe('arrow-up')

        } else if (e.key == 'ArrowDown') {
            this.props.whatevs.exe('arrow-down')
        }
    },
    keyUp: function (e) {
        var name = e.target.value

        if (this.state.selected.name != name) {
            this.setState({ selected: { value: -1, name: name  } })
            this.props.onChange && this.props.onChange(name)
        }
    },
    onBlur: function () {
        //this.props.onCloseOptions && this.props.onCloseOptions() // bug apptimate/safewire-client#1
    },
    tabFocus: function() {
        if (this.jump) {
            this.jump = false

        } else {
            this.props.onOpenOptions && this.props.onOpenOptions()
        }
    },
    render: function() {
        var text = name = this.state.selected.name
        name = name || this.props.hint

        var labelStyle = m(styles.label, text && styles.label_selected)
        var buttonStyle = m(styles.button, this.props.styleBtn, this.state.press && styles.button_press, this.state.press && this.props.styleBtnPress)
        var inputStyle = m(styles.input, { width: this.width })

        var input = this.props.open && <input ref="input" defaultValue={text} style={inputStyle} onKeyDown={this.keyDown} onKeyUp={this.keyUp} onClick={this.stopPropagation} onBlur={this.onBlur}/>

        return (
            <div style={m(styles.control, !this.props.open && this.props.styleCtrl)}>
                <div style={buttonStyle} onMouseUp={this.mouseUp} onMouseDown={this.mouseDown} onMouseOut={this.mouseOut}>
                    <div style={m(styles.arrow, this.props.styleArrow)}></div>
                </div>
                <div ref="label" style={labelStyle} onClick={this.props.onToggleOptions}>
                    <span ref="tab" tabIndex="0" style={styles.tab} onFocus={this.tabFocus}>{name}</span>
                    {input}
                </div>
            </div>
        )
    }
})

var Option = React.createClass({
    getInitialState: function () {
        return { hover: false }
    },
    componentWillUnmount: function () {
        this.props.whatevs.off(null, this)
    },
    componentWillMount: function() {
        this.props.whatevs.on('enter', this, function () {
            if (this.props.active) {
                this.props.onChange(this.props.name)
                this.props.onSelect(this.props.value)
                return true
            }
        })
    },
    onIn: function () {
        this.setState({ hover: true })
    },
    onOut: function () {
        this.setState({ hover: false })
    },
    onClick: function () {
        this.props.onChange(this.props.name)
        this.props.onSelect(this.props.value)
    },
    render: function () {
        var optionStyle = m(styles.option, this.props.styleOptn, this.state.hover && styles.option_hover, this.state.hover && this.props.styleOptnHover, this.props.active && styles.option_active, this.props.active && this.props.styleOptnActive)

        return (
            <div style={optionStyle} data-value={this.props.value} onMouseOver={this.onIn} onMouseOut={this.onOut} onClick={this.onClick}>
                {this.props.name}
            </div>
        )
    }
})

var Options = React.createClass({
    getInitialState: function () {
        return { active: -1 }
    },
    componentWillUnmount: function () {
        this.props.whatevs.off(null, this)
    },
    componentWillMount: function() {
        this.props.whatevs.on('arrow-up', this, function () {
            this.moveUp()
        })
        this.props.whatevs.on('arrow-down', this, function () {
            this.moveDown()
        })
    },
    componentDidMount: function() {
        // console.log('Options did mount, active: ', this.state.active)
    },
    moveUp: function () {
        this.setState({ active: Math.max(-1, this.state.active -1) })
    },
    moveDown: function () {
        this.setState({ active: Math.min(Math.max(0, this.state.active +1), this.props.options.length -1) })
    },
    render: function () {
        var options = this.props.options.map(function (option, index) {
            var active = index == this.state.active
            return <Option key={option.value} active={active} {...this.props} {...option}/>
        }.bind(this))

        return this.props.options.length ? <div style={styles.options}>{options}</div> : null
    }
})

var Select = React.createClass({
    getInitialState: function () {
        return { open: false }
    },
    componentWillUnmount: function () {
        $(document).off('click.decisions')
    },
    componentWillMount: function() {
        $(document).on('click.decisions', function(event) {
            if (!$(event.target).closest(find(this, 'select')).length) {
                this.setState({ open: false })
            }
        }.bind(this))
    },
    openOptions: function () {
        this.setState({ open: true })
    },
    closeOptions: function () {
        this.setState({ open: false })
    },
    toggleOptions: function () {
        this.setState({ open: !this.state.open })
    },
    onSelect: function (value) {
        this.setState({ open: false })
        this.props.onSelect(value)
    },
    render: function () {
        var options = this.state.open && <Options {...this.props } onSelect={this.onSelect}/>

        var selectStyle = m(styles.select, this.state.open && { zIndex: styles.select.zIndex + 2 } )

        return (
            <div ref='select'>
                <div style={selectStyle}>
                    <Control {...this.props} open={this.state.open} onToggleOptions={this.toggleOptions} onCloseOptions={this.closeOptions} onOpenOptions={this.openOptions}/>
                    {options}
                </div>
            </div>
        )
    }
})

/**
 * Usage: <Decisions hint="User" options={users} onSelect={this.didSelectUser} onChange={this.userDidChange}/>
 */

var Decisions = React.createClass({
    getInitialState: function () {
        return { whatevs: this.props.whatevs || new Whatevs() }
    },
    componentWillUnmount: function () {
        this.state.whatevs.off(null, this)
    },
    componentWillMount: function() {
        this.state.whatevs.on('grab-selected', this, function () {
            var selected = this.state.whatevs.exe('-grab-selected')

            if (selected.value < 0) {
                for (var i = 0; i < this.props.options.length; i++) {
                    if (this.props.options[i].name == selected.name) {
                        selected = this.props.options[i]
                    }
                }
            }

            return selected
        })
    },
    onSelect: function (value) {
        for (var i = 0; i < this.props.options.length; i++) {
            if (this.props.options[i].value == value) {
                this.props.onSelect(this.props.options[i])
                this.state.whatevs.exe('select', this.props.options[i])
            }
        }
    },
    render: function () {
        return <Select {...this.props} selected={this.props.selected} onSelect={this.onSelect} whatevs={this.state.whatevs}/>
    }
});

module.exports = Decisions
