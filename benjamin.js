var React = require('react')
var m = require('./merge')

var styles = {}

styles.button = {
    display: 'block',
    // height: 50,
    textDecoration: 'none',
    padding: 15,
    background: '#079DD9',
    color: '#fff',
    fontSize: 14,
    textAlign: 'center',
    cursor: 'pointer'
}

styles.button_press = {
    background: '#037dca'
}

var Benjamin = React.createClass({
    getInitialState: function () {
        return { press: false };
    },

    mouseDown: function () {
        this.setState({ press: true });
    },

    mouseUp: function () {
        this.setState({ press: false });
    },

    render: function () {

        /* this.props.style -> this.props.styleButton*/
        var style = m(styles.button, this.props.styleButton, this.state.press && styles.button_press, this.state.press && this.props.pressStyle);

        return <a style={style} onClick={this.props.onClick} onMouseDown={this.mouseDown} onMouseUp={this.mouseUp}>{this.props.text}</a>
    }
})

module.exports = Benjamin
