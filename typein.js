var React = require('react')
var f = require('./find')
var m = require('./merge')
var W = require('./whatevs')

var styles = {}

styles.input = {
    display: 'block',
    width: '100%',
    padding: '25px 20px 23px 20px',
    border: 'solid 1px #eee',
    borderRadius: 0,
    color: '#333',
    outline: 'none'
}

styles.row = {
    position: 'relative'
}

styles.icon = {
    position: 'absolute',
    top: 23,
    left: 20,
    color: '#999'
}

styles.button_press = {
    background: '#037dca'
}

var Typein = React.createClass({
    getInitialState: function () {
        return { value: this.props.value || '', whatevs: this.props.whatevs || new W }
    },
    componentWillUnmount: function () {
        this.state.whatevs.off(null, this)
    },
    componentWillMount: function() {
        this.state.whatevs.on('grab-typed', this, function () {
            return this.state.value
        })
        this.state.whatevs.on('clear-typed', this, function () {
            this.setState({ value: '' })
        })
    },
    handlePress: function (e) {
        e.key == 'Enter' && this.props.onSubmit && this.props.onSubmit(e.target.value)
    },
    handleChange: function(e) {
        this.setState({ value: e.target.value })
        this.props.onChange && this.props.onChange(e.target.value)
    },
    render: function () {
        var style = m(styles.input, this.props.style)
        var hint = this.props.placeholder || this.props.hint

        var icon = this.props.icon ? <span style={styles.icon} className={this.props.icon}></span> : null

        return (

            <div style={styles.row}>
                {icon}
                <input type={this.props.type || 'text'} className="form-control" placeholder={hint} value={this.state.value} style={m(style, this.props.styleInput)} onKeyDown={this.handlePress} onChange={this.handleChange}/>
            </div>
        )
    }
})

module.exports = Typein
