var objectAssign = require('object-assign');

function merge() {
    var styles = {};

    for (var i = 0; i < arguments.length; i++) {
        if (arguments[i]) {
            objectAssign(styles, arguments[i])
        }
    }

    return styles;
}

module.exports = merge
