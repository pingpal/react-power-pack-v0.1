var Wrapper = function (we, ns) {
    this.we = we
    this.ns = ns
}

Wrapper.prototype.on = function (verb, that, cb) {
    return this.we.on(verb ? this.ns + ':' + verb : verb, that, cb)
}

Wrapper.prototype.off = function (v, t, c) {
    return this.we.off(v ? this.ns + ':' + v : v, t, c)
}

Wrapper.prototype.exe = function (verb) {
    arguments[0] = this.ns + ':' + verb
    return this.we.exe.apply(this.we, arguments)
}

Wrapper.prototype.ns = function (ns) {
    return new Wrapper(this.we, ns)
}

var Whatevs = function () {
    this.cb = {}
}

Whatevs.prototype.on = function (verb, that, cb) {
    cb || (cb = that) || (that = undefined)
    var array = [ cb, that ]
    this.cb[verb] = this.cb[verb] || []
    this.cb[verb].push(array)
    return array
}

Whatevs.prototype.off = function (v, t, c) {
    var cb = this.cb
    for (var k in cb)
    if (cb.hasOwnProperty(k) && (!v || v == k))
    for (var i = 0; i < cb[k].length; i++)
    if ((!t || t == cb[k][i][1]) && (!c || c == cb[k][i][0]))
    this.cb[k].splice(i--, 1)
}

Whatevs.prototype.exe = function (verb) {
    var result, cb = this.cb[verb] || []
    var args = Array.prototype.slice.call(arguments, 1)
    cb.forEach(function (cb) {
        var r = cb[0].apply(cb[1], args)
        if (r !== undefined) result = r
    })
    return result
}

Whatevs.prototype.ns = function (ns) {
    return new Wrapper(this, ns)
}

module.exports = Whatevs
